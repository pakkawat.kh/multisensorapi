<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$frontCover = $_POST['frontCover'];
$info = $_POST['info'];
$overviewChart = $_POST['overviewChart'];
$chart = $_POST['chart'];
$event = $_POST['event'];
$conclusion = $_POST['conclusion'];
$backCover = $_POST['backCover'];
$projectID = $_POST['projectID'];
$daily = $_POST['daily'];
$weekly = $_POST['weekly'];
$monthly = $_POST['monthly'];

$db->delete("autoreport",[
    "projectID"=>$projectID
]);

$db->insert("autoreport",[
    "frontCover"=>$frontCover,
    "info"=>$info,
    "overviewChart"=>$overviewChart,
    "chart"=>$chart,
    "event"=>$event,
    "conclusion"=>$conclusion,
    "backCover"=>$backCover,
    "projectID"=>$projectID,
    "daily"=>$daily,
    "monthly"=>$monthly,
    "weekly"=>$weekly
]);