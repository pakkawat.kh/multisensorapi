<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$id = $_POST['id'];
$axis = $_POST['axis'];
$columnRead = $_POST['columnRead'];
$factor = $_POST['factor'];
$maxLimit =$_POST['maxLimit'];
$minLimit =$_POST['minLimit'];
$sensorid = $_POST['sensorid'];
$unit = $_POST['unit'];

$db->update("axis",[
    "sensorid"=>$sensorid,
    "axis"=>$axis,
    "unit"=>$unit,
    "factor"=>$factor,
    "minLimit"=>$minLimit,
    "maxLimit"=>$maxLimit,
    "columnRead"=>$columnRead
],[
    "id"=>$id
]);
?>