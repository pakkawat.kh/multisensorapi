<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$projectName = $_POST['projectName'];
$shortenURL = $_POST['shortenURL'];
$projectAddress = $_POST['projectAddress'];
$isPassword = $_POST['isPassword'];
$password = $_POST['password'];
$dateTime = $_POST['dateTime'];
$duration = $_POST['duration'];
$lineNumber = $_POST['lineNumber'];
$key = $_POST['key'];

//Check key
$result = $db->select("user","*",[
    "hashkey"=>$key
]);
if(sizeof($result)== 0){
    echo "logout";
} else {
    $db->insert("project",[
        "projectName"=>$projectName,
        "isPassword"=>$isPassword,
        "password"=>$password,
        "shortenURL"=> $shortenURL,
        "address"=>$projectAddress,
        "dateTime"=>$dateTime,
        "duration"=>$duration,           
        "lineNumber"=>$lineNumber
       ]);
   
       echo "finish";
}

?>