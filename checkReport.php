<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$projectID = $_POST['projectID'];
//Check key

$result = $db->select("report","*",[
"projectID"=>$projectID
]);
if(sizeof($result) == 0){
    $db->insert("report",[
        "projectID"=>$projectID,
        "onScreenReport"=>0,
        "autoReport"=>0
    ]);
    $db->delete("autoreport",[
        "projectID"=>$projectID
    ]);
    $db->insert("autoreport",[
        "frontCover"=>0,
        "info"=>0,
        "overviewChart"=>0,
        "chart"=>0,
        "event"=>0,
        "conclusion"=>0,
        "backCover"=>0,
        "projectID"=>$projectID,
        "daily"=>0,
        "monthly"=>0,
        "weekly"=>0
    ]);
    $db->delete("onscreenreport",[
        "projectID"=>$projectID
    ]);
    $db->insert("onscreenreport",[
        "frontCover"=>0,
        "info"=>0,
        "overviewChart"=>0,
        "chart"=>0,
        "event"=>0,
        "conclusion"=>0,
        "backCover"=>0,
        "projectID"=>$projectID,

    ]);
}


?>