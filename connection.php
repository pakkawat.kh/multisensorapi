<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
header('Content-type: application/x-www-form-urlencoded');
header('Content-type: application/json');


date_default_timezone_set("Asia/Bangkok");


// If you installed via composer, just use this code to require autoloader on the top of your projects.
// require 'vendor/autoload.php';
require("Medoo.php");
 
// Using Medoo namespace
use Medoo\Medoo;
 


// $db = new Medoo([
// 	'type' => 'mysql',
// 	'host' => '163.44.198.57',
// 	'port'=>3306,
// 	'database' => 'cp_888493_kodpeak',
// 	'username' => 'cp888493_admin',
// 	'password' => 'chomart12',
// ]);

$db = new Medoo([
	'type' => 'mysql',
	'host' => 'localhost',
	'database' => 'bhmsv2',
	'username' => 'root',
	'password' => '12345678',
	"charset" => "utf8",
]);
// $db = new Medoo([
// 	'type' => 'mysql',
// 	'host' => 'localhost',
// 	'database' => 'cp888493_kodpeak',
// 	'username' => 'cp888493_admin',
// 	'password' => 'chomart12',
// 	"charset" => "utf8",
// ]);
 

$itemperpage = 40;
?>