<?php
require_once('connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$sensorid = $_POST['sensorID'];
$axis = $_POST['axis'];
$unit = $_POST['unit'];
$factor = $_POST['factor'];
$column = $_POST['column'];
$minLimit = $_POST['minLimit'];
$maxLimit = $_POST['maxLimit'];

$db->insert("axis",[
    "sensorid"=>$sensorid,
    "axis"=>$axis,
    "unit"=>$unit,
    "factor"=>$factor,
    "columnRead"=>$column,
    "minLimit"=>$minLimit,
    "maxLimit"=>$maxLimit
]);
?>